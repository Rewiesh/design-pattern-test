package Application;

import dpFactory.GenerateBill;
import dpFactory.GetPlanFactory;
import dpFactory.Plan;
import java.io.*;

public class Application {

    public static void main(String[] args) throws IOException {
        GenerateBill generateBill= new GenerateBill();
        generateBill.run();
    }
}
